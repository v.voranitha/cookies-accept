
## INFOMATION

    แสดง cookie confirm เพื่อให้ user ทราบว่าใน site จะมีการเก็บ cookie และเมื่อคลิกปุ่ม ยอมรับ ระบบจะทำการบันทึก cookies เพื่อไม่ให้แสดง section นี้ขึ้นมาอีกจนกว่าจะหมดอายุ (3ปี)

## PACKAGE
- antd - ^4.16.7
- dayjs - ^1.10.6
- js-cookie - ^2.2.1
- md5 - ^2.3.0

## HOW TO USE

- yarn
- npm install 
