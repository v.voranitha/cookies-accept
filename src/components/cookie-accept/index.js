import { useEffect, useState } from "react";
import { Button, Card } from "antd";
import Cookies from "js-cookie";
import dayjs from "dayjs";
import md5 from "md5";

const KEY_COOKIE = "cookie_confirm";

const CookieAccept = () => {
  const [visible, setVisible] = useState(false);

  // STEP 1 : ค้นหา cookie_accept_id ถ้าเจอจะแสดง Popup
  function openCookieAccept() {
    // ตรวจสอบ cookieId ก่อน
    const cookieId = Cookies.get("cookie_accept_id"); // default = undefined
    console.log({ cookieId });
    // ถ้ามีจะแสดง Notication
    setVisible((prev) => !cookieId && !prev);
  }

  async function handleAccept() {
    try {
      let _id = md5(KEY_COOKIE); // Mock ID
      let _expires = 7; // Expires ID
      let _timeStamp = dayjs(new Date()).format("YYYY-MM-DD HH:MM:ss");

      let data = {
        action: KEY_COOKIE,
        uid: _id,
        origin: window.location.hostname,
        page: window.location.href,
        timestamp: _timeStamp,
      };

      const status_cookie = await AcceptCookie(data);
      if (status_cookie) {
        Cookies.set("cookie_accept_id", _id, { expires: _expires });
        openCookieAccept();
      }
    } catch (error) {
      console.log({ error });
    }
  }

  async function AcceptCookie(payload) {
    try {
      console.log("POST :: ", { ...payload });
      // const response = await fetch();
      /* ------------------------------- API SUCCESS ------------------------------ */
      const response = {
        status: 200,
        data: { success: true, message: "COOKIE ACCEPT SUCCESS" },
      };
      /* ------------------------------- API FAILURE ------------------------------- */
      // const response = { status: 200 , msg:'COOKIE ACCEPT ERROR !!!' };
      console.log("RESPONSE :: ", { ...response });

      if ("msg" in response) {
        throw response.msg;
      }

      console.log(response?.data?.message);
      return response?.data;
    } catch (error) {
      console.log(error);
    }
  }

  async function handleClear() {
    await Cookies.remove("cookie_accept_id");
    await window.location.reload();
  }

  useEffect(() => {
    openCookieAccept();
  }, []);

  return (
    <>
      <div
        style={{
          position: "relative",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          width: "100%",
          marginTop: "3rem"
        }}
      >
        <div>
          <h2> Welcome to Cookie Accept </h2>
        </div>
        <Button
          type="primary"
          shape="round"
          style={{ width: 200 }}
          onClick={() => handleClear()}
        >
          ล้าง Cookies
        </Button>
        <div style={{ padding: 20 }}>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Odit, modi,
          deleniti repellat laboriosam perspiciatis amet vel debitis eaque
          sapiente vero quibusdam dolorem esse earum facere delectus tenetur
          commodi suscipit incidunt? Earum impedit ab doloribus neque eos,
          explicabo incidunt, natus, quod nihil architecto fugit ut dolore
          tenetur quis. Quos, modi ullam? Vitae doloribus, velit fuga
          repudiandae odit ab et odio dolor. Ut temporibus delectus maxime
          incidunt voluptates eos fuga facilis quidem est sequi! Odio reiciendis
          molestiae a vitae totam, laboriosam ipsam! Eligendi, adipisci! Hic
          magni eligendi sapiente beatae pariatur possimus rem. Provident
          cupiditate doloribus voluptatum totam corrupti enim at iure mollitia,
          velit optio error eius autem! Sed saepe iure repudiandae, expedita
          reprehenderit quasi atque magni ea? Tempora suscipit modi neque
          facilis! Delectus, maxime repellat quis aliquid provident officiis
          asperiores porro corporis voluptatem recusandae alias tempore
          doloribus facilis, et hic quod sunt placeat veritatis cumque fugit
          modi necessitatibus reprehenderit obcaecati? Ex, odit. Nobis,
          reiciendis tempora magni dolores a, officia reprehenderit in nam ipsam
          molestias est doloremque praesentium sapiente. Aspernatur, cum non
          iusto reprehenderit eum omnis vitae sunt accusamus voluptatem culpa,
          sequi pariatur. Facilis fugiat nam cupiditate facere sint, laboriosam
          quos! In eos nesciunt, quidem fuga maxime vero necessitatibus veniam
          atque deleniti consectetur distinctio accusamus libero dolorum
          delectus quisquam suscipit consequatur vel ullam! Maiores odit culpa
          voluptate suscipit iste? Delectus, distinctio repudiandae. Neque
          aspernatur reiciendis doloribus natus aperiam praesentium odit. Atque,
          dolorum! Repellat, ullam aperiam. Quos molestiae in ad aspernatur
          voluptatum assumenda ratione. Cupiditate reprehenderit exercitationem
          vel pariatur consequuntur quos eligendi, consectetur eaque rerum
          tempore tempora dolorum, veritatis obcaecati repudiandae earum
          asperiores iure, amet dignissimos! Maiores ratione, perferendis
          quisquam totam dolore necessitatibus reiciendis? Consectetur
          praesentium culpa, quaerat illo non doloremque! Veritatis modi eveniet
          blanditiis soluta officia mollitia quo, doloremque recusandae
          necessitatibus animi quasi aliquid, ipsa labore ipsum quas ducimus
          explicabo, tempora iste incidunt! Quis, consequatur voluptatem. Ipsam
          earum repudiandae mollitia sequi repellendus cupiditate nihil
          perferendis! Qui sunt animi aspernatur voluptas quae tempora
          voluptatum, in, earum voluptatem quaerat reiciendis illum, quod nulla
          odio unde? Alias facere illum quo incidunt iste voluptate, expedita
          ratione deleniti asperiores eum explicabo repellat dicta quae modi
          dolores ex ipsa? Autem nisi rem cupiditate praesentium voluptates sit
          ad ipsum velit. Ipsa veritatis ratione voluptatem eos iusto repellat
          maxime. Reiciendis animi vel hic ullam, quo laborum quasi ab totam
          blanditiis doloremque distinctio ipsam saepe dicta pariatur inventore
          quisquam expedita maiores ut? Dolore vero harum earum unde cum nam
          tempore nemo rem inventore tenetur consequuntur aspernatur laborum
          possimus quia incidunt, reprehenderit, quae, nesciunt ab esse? Debitis
          laborum non cumque, in provident natus! Officia natus, sit vel
          corrupti nemo hic cum ipsam ducimus, fuga quasi nam nobis distinctio
          necessitatibus voluptatem dolorem dolor at. Aspernatur fugiat non
          laboriosam quisquam natus adipisci ut blanditiis aut!
        </div>
      </div>
      {visible && (
        <section id="cookie-accept" style={{ position: "sticky", bottom: 0 , padding: 20}}>
          <Card>
            <h2> Notification Cookie </h2>
            <p>
              เราใช้คุกกี้ (cookie)
              เพื่อพัฒนาประสบการณ์การใช้งานจากการเยี่ยมชมเว็บไซต์ของเราและเพื่อสนับสนุนประสิทธิภาพในการนำเสนอข้อมูลและเนื้อหาต่างๆ
              ที่ผู้เข้าใช้งานจะได้รับชม
            </p>
            <a href="#privacyPage"> รายละเอียดเพิ่มเติม </a>

            <div
              style={{
                display: "flex",
                widht: "100%",
                justifyContent: "flex-end",
              }}
            >
              <Button
                type="primary"
                shape="round"
                onClick={() => handleAccept()}
              >
                ยอมรับ
              </Button>
            </div>
          </Card>
        </section>
      )}
    </>
  );
};

export default CookieAccept;
