import "antd/dist/antd.css";
import { StickyContainer } from 'react-sticky';
import CookieAccept from "./components/cookie-accept";

function App() {
  return(
    <StickyContainer>
      <CookieAccept />
    </StickyContainer>
    );
}

export default App;
